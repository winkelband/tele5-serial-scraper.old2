# Tele5 Serial Scraper

To create comma-seperated values with informations on videos from Tele5.de run:
`scrapy runspider --nolog spider.py`

The following informations get parsed into *tele-5-seriallist.csv*:
- video_serial (name of a serial)
- video_subserial (if serial is a subserial like Star Trek: TNG)
- video_season_no
- video_episode_no
- video_title
- video_uri (link to Tele5 website containing the actual video)

